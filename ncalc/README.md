# CalcoloNumericoEx
Riassunti esercitazioni lezioni di calcolo numerico.

Per visualizzare in locale installare Jekyll:

```
gem install jekyll
```

puntare alla directory del repo ed avviare con

```
jekyll serve -w
```

**Non** utilizzare lo script `posting_website.sh`, e' un metodo per postare, ma lo fa su un account personale, per motivi di testing.
Per visualizzare la versione attuale online [cliccare qui](https://matteoragni.bitbucket.org)
