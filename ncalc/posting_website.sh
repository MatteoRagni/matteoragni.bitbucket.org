#!/bin/bash

cd ../calcolonumerico_2
jekyll build -s ../calcolonumerico -d ./ncalc
git add . --all
git commit -m "builded :: $(date)"
git push $1 master
cd ../calcolonumerico

exit 0
