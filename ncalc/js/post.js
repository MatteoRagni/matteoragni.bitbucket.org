var eval_in_page = [];
var eval_str;
var ret_str;
var mrb = null;
var editor = null;

function redirectConsole() {
	if (typeof console  != "undefined") 
	    if (typeof console.log != 'undefined')
	        console.olog = console.log;
	    else
	        console.olog = function() {};

	console.log = function(message) {
	    console.olog(message);
	    $('#modalContent').append("<code>" + message + "</code><br/>");
	};
	console.error = console.debug = console.info =  console.log
}

// Unfortunatly original code must be parsed against 
// some definition problem in UTF encoding of web page :(
function get_original_code(str) {
	eval_str = str.replace(/<span class="lineno">\s*\d*\s*<\/span>/g,"").
				replace(/<\/?[^>]+(>|$)/g, "").
				replace(/\&gt;/g,">").
	           	replace(/\&lt;/g,"<").
	           	replace(/\&quot/g,'\"').
	           	replace(/\&amp/g,"\&");
	return eval_str;
}

function runEvaluation() {
  eval_str = editor.getValue();

	$('#modalContent').html('');
	mrb.run_source(eval_str);
}

function consoleClear() { 
	$(".modalDialog").css("opacity", "0").css("pointer-events","none");
	setTimeout(function() {$('#modalContent').html('');},500);
}

function eval_ruby(id) {
	get_original_code( $("#" + id).html() );

  editor.setValue(eval_str)
  editor.moveCursorTo(0,0);
  
  // Visualize modal with editor  
	$(".modalDialog").css("opacity", "1").css("pointer-events","auto");
  // Execute evaluation of the code
  mrb.run_source(eval_str);
}

// Load paper effects for each code elements
function loadEval() {
	$("div.highlight").each( function(i) {

		if ($(this).children("pre:first").children("code:first").attr('data-lang') === "ruby") {
			// We need an ID for each code for evaluation purpose.
			// Id is added only to data-lang="ruby" elements
			var _id = "evaluate-pre-" + i;
			eval_in_page.push(_id)
			$(this).children("pre:first").children("code:first").attr('id', _id);

			// Reading code and elimination of not supported tags.
			// For now i can't do this before this javascript code
			add_el = "<button class='eval-btn' id='" + _id + "-btn' onclick='eval_ruby(\"" + _id + "\")'>Try ME!</button>";
			$(this).append(add_el);
		}
	});
}

function toc() {
	//TODO
}

$(document).ready( function() {
  // Setting editor
  editor = ace.edit("modalEditor");
  editor.setTheme("ace/theme/twilight");
  editor.getSession().setMode("ace/mode/ruby");
  // Repl interpreter
  mrb = WEBRUBY();
  redirectConsole();
	loadEval();
	toc();
});